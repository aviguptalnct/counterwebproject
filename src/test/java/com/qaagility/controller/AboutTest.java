package com.qaagility.controller;
import static org.junit.Assert.*;
import org.junit.Test;

public class AboutTest {
    @Test
    public void testDesc() throws Exception {
       String result = new About().desc();
       assertTrue("Incorrect description, it should have contained blah", result.contains("application"));
    }
}
